﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WildWalrus.PathFinding
{
	public static class TerrainDimensions
	{
		#region Variables

		//change to reflect actual terrain bounds
		//where Rect( x pos, z pos, x width, z length ) of terrain
		static Rect terrainDimensions = new Rect( -40f, -10f, 128f, 128 );
		
		public static float x { get { return terrainDimensions.x; } }
		public static float z { get { return terrainDimensions.y; } }
		public static float width { get { return terrainDimensions.width; } }
		public static float height { get { return terrainDimensions.height; } }

		#endregion

		#region Functions
		
		public static float Steepness( Vector3 vector )
		{
			return Mathf.Cos( Terrain.activeTerrain.terrainData.GetSteepness( X( vector.x ), Z( vector.z ) ) * Mathf.Deg2Rad );
		}

		public static float Height( Vector3 vector )
		{
			Debug.Log( "X " + X( vector.x ) );
			Debug.Log( "Z " + Z( vector.x ) );
			return Terrain.activeTerrain.terrainData.GetHeight( X( vector.x ), Z( vector.z ) );
		}

		static int X( float gX )
		{
			return ( int ) ( ( gX - terrainDimensions.x ) / terrainDimensions.width );
		}

		static int Z( float gZ )
		{
			return ( int ) ( ( gZ - terrainDimensions.y ) / terrainDimensions.height );
		}
		
		#endregion
		
	}
	
}
﻿using System.Collections.Generic;
using UnityEngine;

namespace WildWalrus.PathFinding
{
	[ System.Serializable ]

	public class Path
	{
		#region Variables
		
		public float _moveStep;
		public float _riseCost;
		public Vector3 _start;
		public Vector3 _end;
		//Rect _terrainDimensions;
		public PathLeg _directPath;
		public float _run;
		public Vector3 _direction;
		public Vector3 _perpendicular;
		public List< PathLeg > _legs = new List< PathLeg >();
		public float _length;
		public float _moveCost;
		public int _steepestPoint;
		public float _steepestPointRise;
		public float _moveCostToSteepestPoint;
		public float _timeToKeepGizmoLines = 200f;
		public float _maxPathDistance;

		//PathLeg _leg1;
		//PathLeg _leg2;

		//public PathLeg _bestPath;
		
		#endregion

		#region Functions
		
		public Path( float moveStep, float riseCost, Vector3 start, Vector3 end, float maxDistance ) //, Rect terrainDimensions )
		{
			//Debug.Log( "Creating TestPath from " + S.VectorS( start ) + " to " + S.VectorS( end ) );
			_moveStep = moveStep;
			_riseCost = riseCost;
			_start = start;
			_start.y = Terrain.activeTerrain.SampleHeight( _start );
			_end = end;
			_end.y = Terrain.activeTerrain.SampleHeight( _end );
			_maxPathDistance = maxDistance;
			Debug.DrawLine( ( _start + Vector3.up * 0.5f ), ( _start + Vector3.up * 1f ), Color.black, _timeToKeepGizmoLines );
			Debug.DrawLine( ( _end + Vector3.up * 0.5f ), ( _end + Vector3.up * 1f ), Color.white, _timeToKeepGizmoLines );
			Debug.DrawLine( ( _start + Vector3.up * 1f ), ( _end + Vector3.up * 1f ), Color.yellow, _timeToKeepGizmoLines );
			//_terrainDimensions = terrainDimensions;
			_directPath = new PathLeg( _moveStep, _riseCost, _start, _end );//, _terrainDimensions );
		}

		public List< Vector3 > _points = new List< Vector3 >();

		public bool AlternatePaths()
		{
			return AlternatePaths( _directPath );
		}

		//avoid GC... nope doesn't work with recursion!
		//float lowestMoveCost;
		//Vector3[][][] pPoints;
		//int l;
		//int j;
		//PathLeg[,,,] pathLegs;
		//int lowI;
		//int lowJ;
		//int lowK;

		public bool AlternatePaths( PathLeg directPath )
		{
			if( ( _end - _start ).magnitude > _maxPathDistance )
			{
				Debug.Log( "Too far!" );
				directPath.DrawPath();
				AddPoints( directPath );
				return false;
			}

			if( directPath._points.Length < 3 )
			{
				directPath.DrawPath();
				AddPoints( directPath );
				return false;
			}
			
			float lowestMoveCost = directPath._moveCost;
			Vector3[][][] pPoints = new Vector3[2][][];
			pPoints[0] = directPath.PerpendicularPoints();
			pPoints[1] = directPath.PerpendicularPoints1();
			int l = pPoints[0][0].Length;
			PathLeg[,,,] pathLegs = new PathLeg[ 2, 2, l, 2 ];
			int lowI = -1;
			int lowJ = -1;
			int lowK = -1;

			for( int k = 0; k < 2; k ++ )
			{
				for( int i = 0; i < 2; i ++ )
				{
					for( int j = 0; j < l; j ++ )
					{
						pathLegs[ k, i, j, 0 ] = new PathLeg( _moveStep, _riseCost, directPath._start, pPoints[k][i][j] );//, _terrainDimensions );
						//Debug.Log( "_moveCost i = " + i + "  j = " + j + "  is " + pathLegs[ i, j, 0 ]._moveCost );
						pathLegs[ k, i, j, 1 ] = new PathLeg( _moveStep, _riseCost,  pPoints[k][i][j], directPath._end );//, _terrainDimensions );
						//Debug.Log( "_moveCost i = " + i + "  j = " + j + "  is " + pathLegs[ i, j, 1 ]._moveCost );
						float moveCost = pathLegs[ k, i, j, 0 ]._moveCost + pathLegs[ k, i, j, 1 ]._moveCost;
						//Debug.Log( "moveCost at i = " + i + "  j = " + j  + "  k = " + k + "  is " + moveCost );
						if( moveCost < lowestMoveCost )
						{
							//Debug.Log( "moveCost at i = " + i + "  j = " + j  + "  k = " + k + "  is " + moveCost );
							lowestMoveCost = moveCost;
							lowI = i;
							lowJ = j;
							lowK = k;
						}
					}
				}
			}
			
			if( lowI > -1 )
			{
				directPath.AddSubPathLegs( pathLegs[ lowK, lowI, lowJ, 0 ], pathLegs[ lowK, lowI, lowJ, 0 ] );
				AlternatePaths( pathLegs[ lowK, lowI, lowJ, 0 ] );
				AlternatePaths( pathLegs[ lowK, lowI, lowJ, 1 ] );
				return true;
			}
			else
			{
				directPath.DrawPath();
				//_bestPath = directPath;
				//_legs.Add( directPath );
				AddPoints( directPath );
				return false;
			}
		}

		void AddPoints( PathLeg pathLeg )
		{
			for( int i = 0; i < pathLeg._points.Length; i ++ )
			{
				_points.Add( pathLeg._points[i]._point );
			}
			//Debug.Log( "_points.Count " + _points.Count );
		}
		
		/*
		public bool AlternatePaths_1( TestPathLeg directPath )
		{
			if( directPath._points.Length < 3 )
			{
				directPath.DrawPath();
				return false;
			}
			
			float lowestMoveCost = directPath._moveCost;
			Vector3[][][] pPoints = new Vector3[2][][];
			pPoints[0] = directPath.PerpendicularPoints();
			pPoints[1] = directPath.PerpendicularPoints1();
			int l = pPoints[0].Length;
			TestPathLeg[,,,] pathLegs = new TestPathLeg[ 2, 2, l, 2 ];
			int lowI = -1;
			int lowJ = -1;
			int lowK = -1;
			
			for( int k = 0; k < 2; k ++ )
			{
				for( int i = 0; i < 2; i ++ )
				{
					for( int j = 0; j < l; j ++ )
					{
						pathLegs[ k, i, j, 0 ] = new TestPathLeg( _moveStep, _riseCost, directPath._start, pPoints[k][i][j], _terrainDimensions );
						//Debug.Log( "_moveCost i = " + i + "  j = " + j + "  is " + pathLegs[ i, j, 0 ]._moveCost );
						pathLegs[ k, i, j, 1 ] = new TestPathLeg( _moveStep, _riseCost,  pPoints[k][i][j], directPath._end, _terrainDimensions );
						//Debug.Log( "_moveCost i = " + i + "  j = " + j + "  is " + pathLegs[ i, j, 1 ]._moveCost );
						float moveCost = pathLegs[ k, i, j, 0 ]._moveCost + pathLegs[ k, i, j, 1 ]._moveCost;
						Debug.Log( S.VectorS( pPoints[k][i][j] ) );
						//Debug.Log( "moveCost at i = " + i + "  j = " + j  + "  k = " + k + "  is " + moveCost );
						if( moveCost < lowestMoveCost )
						{
							Debug.Log( "moveCost at i = " + i + "  j = " + j  + "  k = " + k + "  is " + moveCost );
							lowestMoveCost = moveCost;
							lowI = i;
							lowJ = j;
							lowK = k;
						}
					}
				}
			}

			if( lowI > -1 )
			{
				directPath.AddSubPathLegs( pathLegs[ lowK, lowI, lowJ, 0 ], pathLegs[ lowK, lowI, lowJ, 0 ] );
				AlternatePaths( pathLegs[ lowK, lowI, lowJ, 0 ] );
				AlternatePaths( pathLegs[ lowK, lowI, lowJ, 1 ] );
				return true;
			}
			else
			{
				directPath.DrawPath();
				return false;
			}
		}
		*/

		#endregion
		
	}
	
}
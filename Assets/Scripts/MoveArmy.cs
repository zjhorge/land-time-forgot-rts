﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WildWalrus.PathFinding;

namespace LandTimeForgot
{
	public class MoveArmy : MonoBehaviour
	{
		#region Variables
		
		[ SerializeField ] Transform _transform;
		[ SerializeField ] Animator _animator;

		[ SerializeField ] float _speed = 3f;
		[ SerializeField ] float _stopDistance = 0.1f;
		float _stopDistSq;

		Path _path;

		[ SerializeField ] bool _isMoving = false;

		Vector3 _targetPosition;
		
		int _speedHash;

		#endregion

		#region Functions

		public Vector3 GetPosition()
		{
			return _transform.position;
		}
		
		void Start()
		{
			_speedHash = Animator.StringToHash( "Speed" );
			_stopDistSq = _stopDistance * _stopDistance;//Compare squareMag is faster
		}
		
		void Update()
		{
			if( _isMoving )
			{
				UpdateMove();
			}
		}
		
		public void MoveAlongPath( Path path )
		{
			if( IsAtEndOfPath( path ) )
			{
				return;
			}

			_path = path;
			point = 1;

			_targetPosition = _path._points[ point ];
			_isMoving = true;
			_animator.SetFloat( _speedHash, 1f );
		}
		
		// avoid GC
		int subLeg;
		int point;

		void UpdateMove()
		{
			if( IsAtEndOfPath() )
			{
				_transform.position = _path._end;
				_isMoving = false;
				_animator.SetFloat( _speedHash, 0f );
			}
			else
			{
				if( IsCloseEnough( _transform.position, _targetPosition ) )
				{
					//set the next target point
					NextTargetPoint();
				}
				else
				{
					//move to next point
					Rotate();
					Move();
				}
			}
		}

		bool IsAtEndOfPath( Path path )
		{
			if( path == null )
				return true;
			return ( ( path._end - _transform.position ).sqrMagnitude < _stopDistSq );
		}

		bool IsAtEndOfPath()
		{
			return ( ( _path._end - _transform.position ).sqrMagnitude < _stopDistSq );
		}

		bool IsCloseEnough( Vector3 from, Vector3 to )
		{
			return ( ( to - from ).sqrMagnitude < _stopDistSq );
		}

		void NextTargetPoint()
		{
			point ++;
			_targetPosition = _path._points[ point ];
		}

		void Move()
		{
			_transform.Translate( _transform.forward * _speed * Time.deltaTime, Space.World );
			_transform.position = new Vector3( _transform.position.x, Terrain.activeTerrain.SampleHeight( _transform.position ), _transform.position.z );
		}

		Vector3 lookAtPos;

		void Rotate()
		{
			lookAtPos = _targetPosition - _transform.position;
			lookAtPos.y = 0f;
			_transform.rotation = Quaternion.LookRotation( lookAtPos );
		}

		#endregion
		
	}
	
}
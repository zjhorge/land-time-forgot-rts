﻿using UnityEngine;

namespace WildWalrus.PathFinding
{
	//[ System.Serializable ]

	public class PathLeg
	{
		#region Variables
		
		//Rect _terrainDimensions;
		
		public float _moveStep;
		public float _riseCost;
		public Vector3 _start;
		public Vector3 _end;
		//public float _rise;
		public float _run;
		public Vector3 _direction;
		public Vector3 _perpendicular;
		public float _lastStep;
		public PathPoint[] _points;
		public float _length;
		public float _moveCost;
		public int _steepestPoint;
		public float _steepestPointRise;
		public float _moveCostToSteepestPoint;
		public PathLeg[] _subPathLegs;
		public float _timeToKeepGizmoLines = 200f;

		#endregion

		#region Functions
		
		public PathLeg( float moveStep, float riseCost, Vector3 start, Vector3 end )//, Rect terrainDimensions )
		{
			//Debug.Log( "Creating TestPathLeg from " + S.VectorS( start ) + " to " + S.VectorS( end ) );
			_moveStep = moveStep;
			_riseCost = riseCost;
			_start = start;
			_start.y = Terrain.activeTerrain.SampleHeight( _start );
			_end = end;
			_end.y = Terrain.activeTerrain.SampleHeight( _end );
			//_terrainDimensions = terrainDimensions;
			Points();
		}
		
		public PathPoint[] Points()
		{
			Vector3 start = _start;
			Vector3 end = _end;
			start.y = 0f;
			end.y = 0f;
			_run = Vector3.Magnitude( end - start );
			_direction = ( end - start ) / _run;
			_perpendicular = Vector3.Cross( start - _start, end - _start );
			_perpendicular.Normalize();
			
			if( _run <= _moveStep * 1.5f )
			{
				_lastStep = _run;
				_points =  new PathPoint[1];
				_points[0] = new PathPoint( _lastStep, _riseCost, _direction, _perpendicular );//, _terrainDimensions );
				_points[0].Point( start, 0 );
				_points[0].Rise( _start.y );
				_length = _points[0].Length( _start );
				_moveCost = _points[0].MovementCost();
				_steepestPoint = 0;
				_steepestPointRise = _points[0]._rise;
				_moveCostToSteepestPoint = _moveCost;
				return _points;
			}
			
			int l = ( int )( _run / _moveStep );
			_lastStep = _run - ( _moveStep * l );
			
			if( _lastStep < ( 0.5f * _moveStep ) )
			{
				_lastStep += _moveStep;
				l --;
			}
			
			_points =  new PathPoint[ l + 1 ];
			_length = 0f;
			_moveCost = 0f;
			_steepestPoint = 0;
			_steepestPointRise = 0f;
			_moveCostToSteepestPoint = 0f;
			Vector3 previousPoint = _start;
			
			for( int i = 0; i < l; i ++ )
			{
				PathPoint pathPoint = new PathPoint( _moveStep, _riseCost, _direction, _perpendicular );//, _terrainDimensions );
				pathPoint.Point( start, i );
				pathPoint.Rise( previousPoint.y );
				_length += pathPoint.Length( previousPoint );
				_moveCost += pathPoint.MovementCost();
				
				if( pathPoint.LargestRise( _steepestPointRise ) )
				{
					_steepestPoint = i;
					_steepestPointRise = pathPoint._rise;
					_moveCostToSteepestPoint = _moveCost;
				}
				
				_points[i] = pathPoint;
				Debug.DrawLine( ( previousPoint + Vector3.up * 0.1f ), ( pathPoint._point + Vector3.up * 0.1f ), Color.magenta, 0.5f );
				Debug.DrawLine( ( pathPoint._point + Vector3.up * 0.1f ), ( pathPoint._point + Vector3.up * 0.3f ), Color.magenta, 0.5f );
				previousPoint = pathPoint._point;
			}
			
			_points[l] = new PathPoint( _lastStep, _riseCost, _direction, _perpendicular );//, _terrainDimensions );
			_points[l]._point = _end;
			_points[l].Rise( previousPoint.y );
			Debug.DrawLine( ( previousPoint + Vector3.up * 0.1f ), ( _points[l]._point + Vector3.up * 0.1f ), Color.magenta, 0.5f );
			
			if( _points[l].LargestRise( _steepestPointRise ) )
			{
				//_steepestPoint = l;
				//_steepestPointRise = _endPoint._rise;
				//_moveCostToSteepestPoint = _moveCost;
			}
			
			_length += _points[l].Length( previousPoint );
			_moveCost += _points[l].MovementCost();
			return _points;
		}
		
		public Vector3[][] PerpendicularPoints()
		{
			return PerpendicularPoints( SearchRange(), _points.Length / 2 );
		}
		
		public Vector3[][] PerpendicularPoints1()
		{
			return PerpendicularPoints( SearchRange(), _steepestPoint );
		}
		
		public Vector3[][] PerpendicularPoints( float range, int index )
		{
			//range will be the amount on either side
			int l = ( int )( range / _moveStep ) + 1;
			Vector3[][] points = new Vector3[2][];
			Vector3 start = _points[ index ]._point;
			
			for( int i = 0; i < 2; i ++ )
			{
				float direction = ( 0.5f - ( float )i ) * 2f;
				points[i] = new Vector3[l];
				//Debug.Log( " direction = " + direction );
				
				for( int j = 0; j < l; j ++ )
				{
					Vector3 point;
					point = start + ( _perpendicular * _moveStep * ( j + 1 ) * direction );
					//Debug.DrawLine( ( point + Vector3.up * 0.1f ), ( point + Vector3.up * 0.3f ), Color.red, 10f );
					points[i][j] = point;
				}
			}
			
			return points;
		}
		
		public float SearchRange()
		{
			Vector3 heading = _end - _start;
			float directLength = Vector3.Magnitude( heading );
			//Vector3 direction = heading / directMagnitude;
			float length1 = ( _steepestPoint + 1 ) * _moveStep / _run * directLength;
			float length2 = directLength - length1;
			float range = Mathf.Sqrt( _moveCost * _moveCost - length1 * length1 - length2 * length2 ) / 2f;
			//float range = Mathf.Sqrt( _length * _moveCost - length1 * length1 - length2 * length2 ) / 2f;
			//Debug.Log( "Search range = " + range );
			range = Mathf.Clamp( range, 0f, 15f );
			return range;
		}
		
		public void AddSubPathLegs( PathLeg pathLeg1, PathLeg pathLeg2 )
		{
			_subPathLegs =  new PathLeg[]{ pathLeg1, pathLeg2 };
		}
		
		public void DrawPath()
		{
			Vector3 previousPoint = _start;
			Debug.DrawLine( ( _start + Vector3.up * 0.01f ), ( _start + Vector3.up * 0.5f ), Color.green, _timeToKeepGizmoLines );
			Debug.DrawLine( ( _end + Vector3.up * 0.01f ), ( _end + Vector3.up * 0.5f ), Color.blue, _timeToKeepGizmoLines );
			Debug.DrawLine( ( _start + Vector3.up * 0.5f ), ( _end + Vector3.up * 0.5f ), Color.cyan, _timeToKeepGizmoLines );
			int l = _points.Length;
			
			for( int i = 0; i < l; i ++ )
			{
				Debug.DrawLine( ( previousPoint + Vector3.up * 0.01f ), ( _points[i]._point + Vector3.up * 0.01f ), Color.red, _timeToKeepGizmoLines );
				previousPoint = _points[i]._point;
			}
			
			//Debug.DrawLine( ( previousPoint + Vector3.up * 0.1f ), ( _endPoint._point + Vector3.up * 0.1f ), Color.red, 10f );
		}
		
		#endregion
		
	}
	
}
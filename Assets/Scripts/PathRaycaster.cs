﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LandTimeForgot;

namespace WildWalrus.PathFinding
{
	public class PathRaycaster : MonoBehaviour
	{
		#region Variables
		
		[ SerializeField ] Camera _camera;
		[ SerializeField ] PathController _pathController;
		[ SerializeField ] LayerMask _layerMask;
		[ SerializeField ] MoveArmy _moveArmy;

		Ray _ray;

		#endregion

		#region Functions
		
		void Update()
		{
			_ray = _camera.ScreenPointToRay( Input.mousePosition );
			RaycastHit hit;

			//Debug.DrawRay( _camera.ScreenToWorldPoint( new Vector3( Input.mousePosition.x, Input.mousePosition.y, _camera.nearClipPlane ) ), _ray.direction * 50f, Color.red );
			
			if( Physics.Raycast( _ray, out hit, _layerMask ) )
			{
				int layer = hit.collider.gameObject.layer;
				//Debug.Log( "Hit layer " + layer + " " + hit.collider.gameObject.name );
				
				if( Input.GetButtonDown( "Fire2" ) )
				{
					//Debug.Log( "Hit layer " + layer );
					switch( layer )
					{
						case 8 :
							Debug.Log( "Army Layer" );
							break;
						case 10 :
							//Debug.Log( "Map Layer" );
							MapClick( hit.point );
							break;
					}
				}
			}
		}
		
		void MapClick( Vector3 point )
		{
			if( _pathController.FindPathFromTo( _moveArmy.GetPosition(), point ) )
			{
				_moveArmy.MoveAlongPath( _pathController.GetPath() );
				_pathController.DrawPath();
			}
			else
			{

			}
		}
		
		#endregion
		
	}
	
}
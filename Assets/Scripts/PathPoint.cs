﻿using UnityEngine;

namespace WildWalrus.PathFinding
{
	//[ System.Serializable ]

	public class PathPoint // is a point that starts at another point _flatLength away on the xz plane in direction opposite to direction
	{
		#region Variables

		//Rect _terrainDimensions;
		
		public float _run;//the path move step, the flat xz distance without including the y height component
		public Vector3 _direction;//the direction the line that this point is a part of travels in, y is always 0f
		public Vector3 _perpendicular;//the perpendicular to the line that this point is a part of travels in, y is always 0f
		public Vector3 _point;
		public Vector2 _terrainPoint;
		public float _length;
		public float _rise;//the difference in y between this point and the previous point
		public float _moveCost;//the rise adjusted cost to traverse this point in the path,
		//and potentially in later revisions the terrain cost and downward slope bonus
		public float _riseCost;
		
		#endregion

		#region Functions
		
		public PathPoint( float run, float riseCost, Vector3 direction, Vector3 perpendicular )//, Rect terrainDimensions )
		{
			_run = run;
			_riseCost = riseCost;
			_direction = direction;
			_perpendicular = perpendicular;
			//_terrainDimensions = terrainDimensions;
		}
		
		public Vector3 Point( Vector3 start, int i )
		{
			_point = start + _direction * _run * ( i + 1 );
			_point.y = Terrain.activeTerrain.SampleHeight( _point );
			//Debug.Log( _point.x + "   " + _point.y + "   " + _point.z );
			return _point;
		}
		
		public float Rise( float previousYPoint )
		{
			_rise = _point.y - previousYPoint;
			return _rise;
		}
		
		public bool LargestRise( float largestRise )
		{
			if( _rise > largestRise )
			{ return true; }
			{ return false; }
		}
		
		public float Length( Vector3 previousPoint )
		{
			_length = Vector3.Magnitude( _point - previousPoint );
			//float length = Mathf.Sqrt( _run * _run + _rise * _rise );//this works and might be faster
			//Debug.Log( "_length = " + _length + "   length = " + length );
			return _length;
		}
		
		public float MovementCost()
		{
			float x = ( _point.x - TerrainDimensions.x ) * TerrainDimensions.width;
			float z = ( _point.z - TerrainDimensions.z ) * TerrainDimensions.height;
			float steepness = Terrain.activeTerrain.terrainData.GetSteepness( x, z );
			//if( steepness > 30f ){ return 500f; }
			steepness = Mathf.Cos( Mathf.Deg2Rad * steepness );
			//_moveCost = _length + steepness;//TerrainDimensions.Steepness( _point );
			_moveCost = _length;
			
			if( _rise > 0f )
			{
				_moveCost += Mathf.Pow( _rise / _run * _length * _riseCost * steepness, 3 );// * steepness * steepness * steepness;
				//_moveCost += steepness;
			}
			else
			{
				//_moveCost += Mathf.Abs( _rise / _run * _length * _riseCost * 0.5f )  * steepness;
				_moveCost += Mathf.Pow( Mathf.Abs( _rise / _run ) * _length * _riseCost * steepness, 0.5f );
			}
			
			return _moveCost;
		}
		
		/*
		public float MovementCost_1()
		{
			float x = ( _point.x - _terrainDimensions.x ) * _terrainDimensions.width;
			float z = ( _point.z - _terrainDimensions.y ) * _terrainDimensions.height;
			float steepness = Terrain.activeTerrain.terrainData.GetSteepness( x, z );
			
			if( steepness > 30f )
			{
				return 500f;
			}
			
			steepness = Mathf.Cos( Mathf.Deg2Rad * steepness );
			//_moveCost = _length + steepness;//TerrainDimensions.Steepness( _point );
			_moveCost = _length;
			
			if( _rise > 0f )
			{
				_moveCost += Mathf.Pow( _rise / _run * _length * _riseCost, 1 ) * steepness * steepness * steepness;
				//_moveCost += steepness;
			}
			else
			{
				//_moveCost += Mathf.Abs( _rise / _run * _length * _riseCost * 0.5f )  * steepness;
				_moveCost += Mathf.Pow( Mathf.Abs( _rise / _run ) * _length * _riseCost, 0.5f ) * steepness;
			}

			return _moveCost;
		}
		*/

		#endregion

	}

}
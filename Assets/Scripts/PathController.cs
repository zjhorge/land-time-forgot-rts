﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WildWalrus.PathFinding
{
	public class PathController : MonoBehaviour
	{
		#region Variables

		float _moveStep = 0.06125f;
		float _riseCost = 20f;
		[ SerializeField ] Path _path;
		Rect _terrainDimensions = new Rect( -40f, -10f, 128f, 128 );

		[ SerializeField ] float _maxDistance = 10f;

		[ SerializeField ] LineRenderer _lineRenderer;

		#endregion

		#region Functions

		public bool FindPathFromTo( Vector3 from, Vector3 to )
		{
			TerrainDimensions.Steepness( to );

			_terrainDimensions.width = 1 / _terrainDimensions.width;
			_terrainDimensions.height = 1 / _terrainDimensions.height;

			_path = new Path( _moveStep, _riseCost, from, to, _maxDistance );

			return _path.AlternatePaths();
		}

		public Path GetPath()
		{
			return _path;
		}
		Vector3[] _pointsArray;
		public void DrawPath()
		{
			//Debug.Log( "DrawPath " + _path._points.Count );
			//Debug.Log( "DrawPath " + _path._points.ToArray().Length );
			_lineRenderer.positionCount = _path._points.Count;
			_lineRenderer.SetPositions( _path._points.ToArray() );
		}
		
		#endregion
		
	}
	
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WildWalrus.PathFinding;

namespace LandTimeForgot
{
	public class CameraController1 : MonoBehaviour
	{
		#region Variables
		
		/*
		[ SerializeField ] Transform _transform;

		[ SerializeField ] CharacterController _characterController;

		[ SerializeField ] Transform _cameraHeightTransform;

		[ SerializeField ] Transform _cameraGroundTransform;

		Vector3 _cameraHeight;

		Vector3 _speed = Vector3.zero;

		[ SerializeField ] float _scrollSpeed = 1f;

		[ SerializeField ] float _maxHeight = 2f;

		[ SerializeField ] float _minHeight = -0.95f;

		[ SerializeField ] float _maxPitch = 45f;

		[ SerializeField ] float _minPitch = 30f;

		[ SerializeField ] float _heightSpeed;

		[ SerializeField ] float _height = 0f;
		
		[ SerializeField ] float _pitch;

		[ SerializeField ] float _turnSpeed = 90f;

		[ SerializeField ] float _gX = 24f;
		
		[ SerializeField ] float _gZ = 54f;

		[ SerializeField ] float _yMin = 0f;

		[ SerializeField ] float _yMax = 10f;
		*/

		[ SerializeField ] Transform _transform;
		[ SerializeField ] Transform _pitchTransform;

		[ SerializeField ] float _scrollSpeed = 1f;
		[ SerializeField ] float _turnSpeed = 90f;
		[ SerializeField ] float _heightScrollSpeed = 1f;

		[ SerializeField ] float _height = 2f;
		[ SerializeField ] float _heightGoal = 0f;
		[ SerializeField ] float _maxHeight = 4f;
		[ SerializeField ] float _minHeight = 0f;
		// how much the camera faces downward when it rises
		[ SerializeField ] float _maxPitch = 60f;
		[ SerializeField ] float _minPitch = 30f;
		[ SerializeField ] float _pitch;

		#endregion

		#region Functions

		/*
		void Start()
		{
			_gX = _cameraGroundTransform.localPosition.x;
			_gZ = _cameraGroundTransform.localPosition.z;
			_cameraHeight = new Vector3( _gX, _height, _gZ );
			_cameraGroundTransform.localPosition = _cameraHeight;
			UpdatePitch();
			Debug.Log( _transform.position.y );
		}
		
		void Update()
		{
			UpdateHeight();
			UpdatePitch();
			UpdateRotation();
			UpdateMove();
		}

		void LateUpdate()
		{
			//keep the camera at the current  unless clipping will occur, then move it up
			if( ! _characterController.isGrounded )
			{
				if( _transform.position.y < _yMin )
				{
					_transform.position = new Vector3( _transform.position.x, _yMin, _transform.position.z );
				}
			}
		}

		void UpdateHeight()
		{
			_height -= Input.GetAxis( "Mouse ScrollWheel" ) * _heightSpeed;
			_height = Mathf.Clamp( _height, _minHeight, _maxHeight );
			Debug.Log( "_height " + _height );
			_cameraHeight = new Vector3( 0f, _height, 0f );
			_cameraHeightTransform.localPosition = _cameraHeight;
		}

		void UpdatePitch()
		{
			_pitch = ( _maxPitch - _minPitch ) * ( _height - _minHeight ) / ( _maxHeight - _minHeight ) + _minPitch;
			_cameraHeightTransform.localEulerAngles = new Vector3( _pitch, 0f, 0f );
		}

		void UpdateRotation()
		{
			_transform.Rotate( 0f, _turnSpeed * Time.deltaTime * Input.GetAxis( "Turning" ), 0f );
		}

		void UpdateMove()
		{
			_speed = _transform.forward * Input.GetAxis( "Vertical" ) + _transform.right * Input.GetAxis( "Horizontal" );
			_speed *= _scrollSpeed;
			_characterController.SimpleMove( _speed );
			//_transform.position = new Vector3(
			//	_transform.position.x,
			//	Mathf.Max( _transform.position.y, _height ),
			//	_transform.position.z );
		}
		*/

		void Update()
		{
			UpdateRotation();
			UpdateMove();
			UpdateHeight();
			UpdatePitch();
		}

		void UpdatePitch()
		{
			_pitch = ( _maxPitch - _minPitch ) * ( _height - _minHeight ) / ( _maxHeight - _minHeight ) + _minPitch;
			_pitchTransform.localEulerAngles = new Vector3( _pitch, 0f, 0f );
		}

		void UpdateRotation()
		{
			_transform.Rotate( 0f, _turnSpeed * Time.deltaTime * Input.GetAxis( "Turning" ), 0f );
		}
		
		void UpdateMove()
		{
			_transform.Translate( ( _transform.forward * Input.GetAxis( "Vertical" ) + _transform.right * Input.GetAxis( "Horizontal" ) ) * _scrollSpeed * Time.deltaTime, Space.World );
			
		}

		//Avoid GC
		float hDel;
		float hTer;

		void UpdateHeight()
		{
			hDel = - Input.GetAxis( "Mouse ScrollWheel" ) * _heightScrollSpeed;
			hTer = Terrain.activeTerrain.SampleHeight( _transform.position );

			_heightGoal += hDel;
			_heightGoal = Mathf.Clamp( _heightGoal, _minHeight, _maxHeight );

			// avoid delay in rising when camera height is lower than terrain height
			if( hDel > 0f && _heightGoal < hTer )
			{
				_heightGoal = hTer + hDel;
			}
			
			_height = Mathf.Max( _heightGoal, Terrain.activeTerrain.SampleHeight( _transform.position ) );
			_transform.position = new Vector3( _transform.position.x, _height, _transform.position.z );
		}

		#endregion
		
	}
	
}